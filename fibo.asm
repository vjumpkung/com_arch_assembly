addi x28,x0,1 # make x28 as 1 for comparision
addi x10,x10,15 # input number
 
main:
	jal x1,fibo # call fibo labels
	j exit # for exit program
	
fibo: 
	addi x2,x2,12
	sw x10,4(x2)
	sw x1,0(x2)
	j IF1 # jump to IF1 labels

IF1:
	beq x10,x28,R1 # check if x10 == 1
	j IF2 # jump to IF2 labels

R1:
	addi x9,x9,1 # count every return 1 to x9
	addi x2,x2,-12
	jalr x0,0(x1) # return

IF2:
	blez x10,R0 # else if x10 <= 0
	j ELSE # jump to ELSE label

R0:
	addi x9,x9,0 # count every return 0 to x9
	addi x2,x2,-12
	jalr x0,0(x1) # return

ELSE:

	addi x10,x10,-1  # fibo(x10-1)
	jal x1,fibo
	
	# get number from memory
	lw x10,4(x2)
	
	# fibo(x10-2)
	addi x10,x10,-2  # fibo(x10-2)
	jal x1,fibo
	
	lw x1,0(x2)

	addi x2,x2,-12
	addi x11,x9,0 # move number to x11 for result
	jalr x0,0(x1)
	
exit: # program end

